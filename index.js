var express = require('express');
var app = express();

var tickerValue = require('./routes/value-router.js');
var tickerMM10 = require('./routes/mm10-router.js');


//Return last value given the Ticker ex. http://localhost:3000/ticker/APPL/value
app.use('/ticker/:ticknum/value', function (req, res, next) {
    req.ticker_config = {
        name: 'ticknum',
        value: req.params.ticknum
    };
    next();
}, tickerValue);


//Return last value given the Ticker ex. http://localhost:3000/ticker/APPL/mm10
app.use('/ticker/:ticknum/mm10', function (req, res, next) {
    req.ticker_config = {
        name: 'ticknum',
        value: req.params.ticknum
    };
    next();
}, tickerMM10);

app.listen(3000);